# Ideas to work on:

- "In my field/domain, we never look back on our source code."
- "We don't need to share our source code outside our team."
- "Sharing source code is fine, but only if the source code is well written. How does SWH deal with source code quality?"
- "I'd rather save/find the executable version of a software."
- "Why should I use a SWHID? DOIs are just fine."
- "My source code doesn't deserve to be archived. It's just a tiny script."
- "It's already on Zenodo."
- "Computing a SWHID may be nice. But many people are not able to do it. Therefore, why does SWH provide this type of PID?"
- "SWH deals only with dead source code."
- "Why archiving the development history if commits are poorly written?"
- "Nobody correctly cites softwares in academic papers. Why should I take care of the metadata of my software if people only refer to it via a link to my repo?"