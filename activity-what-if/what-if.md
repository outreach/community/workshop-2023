# "What if I told you that..." : debunking preconceived ideas on SWH in Academia

Lead: Sabrina Granger & Pierre Poulain

[TOC]

Minutes of the workshop: https://hedgedoc.softwareheritage.org/8Rsxd0lRSniw3x1NLIGfCg?view

## Introduction phase (the why)
* Contents: as an ambassador or SWH enthusiast, we may need to debunk preconceived ideas on SWH. By working together during this short session, we'll strive to improve our ability to find effective arguments and images that help us to foster a better understanding of SWH.

The contents may be added to the SWH FAQ and/or be used as pedagogical resources in other formats. 

We'll work together on a list of preconceived ideas we'd like to fight, but also, a list of the points of view that we'd like to change (i.e. a point of view is not necessarily wrong). 

* Duration: pich the activity in 5' 
* No material needed
* Maximal number of attendees: 6

## Define the activity roles
Choose a note taker and a presenter for the report back
Ask the attendees if someone could: 
- [X] take notes, using the template: Sabrina and Pierre https://hedgedoc.softwareheritage.org/8Rsxd0lRSniw3x1NLIGfCg?view
- [X] present the work done during the report back in plenary session: Pierre
 
## Designing your key messages
 

Ideas to work on 
1. "In my field/domain, we never look back on our source code."
2. "We don't need to share our source code outside our team."
3. "Sharing source code is fine, but only if the source code is well written. How does SWH deal with source code quality?"
4. "I'd rather save/find the executable version of a software."
5. "Why should I use a SWHID? DOIs are just fine."
6. "My source code doesn't deserve to be archived. It's just a tiny script."
7. "It's already on Zenodo."
8. "Computing a SWHID may be nice. But many people are not able to do it. Therefore, why does SWH provide this type of PID?"
9. "SWH deals only with dead source code."
10. "Why archiving the development history if commits are poorly written?"
11. "Nobody correctly cites softwares in academic papers. Why should I take care of the metadata of my software if people only refer to it via a link to my repo?"

The group members pick up the sentences they would like to work on. 
The group starts to work on the sentences that received the maximum number of votes. 

**For each sentence**, the work is organised in 4 stages:

1. Individual work: 2'
2. Gathering ideas with another group member: 2'
3. Plenary session to present the ideas conveyed by the sub-groups: 10'
4. Summarizing step: the key ideas have to be summarized in 1 sentence; the work is done in plenary session

* Material: 
- [X] printed copies of the list of ideas
- [X] pad to gather the ideas of the group


## Final exchanges and closing of the activity
* Contents: define our next steps 
* Duration: 5' after the session
* Materials: notes from the activity on hedgedoc and/or open issues in FAQ repository
https://gitlab.softwareheritage.org/outreach/swh-academy/swh-faq/

## Total duration
Can't exceed 1H

## List of all the materials needed
Attendees are advised to come with their laptop but we can't be sure that there will be a laptop/guest
- [ ] Printed copies of the list of the ideas: for attendees
- [ ] pad to gather the ideas of the group



