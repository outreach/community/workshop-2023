# The Software Heritage module in The Carpentries 
Proposal of canvas for the community event activity

[TOC]

## Introduction
* Contents: A small group will discuss how to include SWH in existing workshops.
* Lead: Joenio
* Plenary: 10'
* Materials: slides https://joenio.me/swh-symposium-and-summit-2023

## Choose a note taker
Ask the attendees if someone could: 
- [ ] take notes, using the template : https://hedgedoc.softwareheritage.org/L7a-nSMhQC-C7NfVbzLf3w#

1 person needed. 

## Preliminary exchanges to start the activity (the how)
* Contents: feedbacks from the audience on the SWH topics, concepts and features to add on Carpentries lessons accordingly the profile of the Carpentries audience
* Lead: Joenio
* Plenary: 10'
* Materials: collaboration on this document https://hedgedoc.softwareheritage.org/the-carpentries-trainning
* Proposal to provide guidance for the exchanges: taking as a starting point to analyse the existing contents the ARDC principles: Archive, Reference, Describe, Cite

## Start the activity
* Contents: select one item from the list of topics and add new content to the lesson selected accordingly the priority; send the contribution to the carpentries
* Lead: Joenio
* Plenary: 20'
* Materials: computer, github, git, markdown https://github.com/LibraryCarpentry/lc-fair-research/

## Final exchanges and closing of the activity
* Contents: *plan the steps after the workshop to introduce the modules to the carpentries*
* Lead: Joenio
* Plenary: 5'
* No Materials

## Total duration
Can't exceed 1H

## List of all the materials needed
Attendees are advised to come with their laptop but we can't be sure that there will be a laptop/guest
