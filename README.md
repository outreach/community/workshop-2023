# SWH Symposium and summit 2023 community workshop

Preparing SWH Symposium and summit 2023 (#SWH_symposium_2023)

Event day: Tuesday, February 7th at the UNESCO headquarters
Starts at: 14.30
Ends at: 19.00 (at the latest)

## Quick links
- Morning plenary: https://www.softwareheritage.org/symposium-and-summit-2023/
- SWH website: https://www.softwareheritage.org/
- SWH archive: https://archive.softwareheritage.org/
- Template for note taking: https://hedgedoc.softwareheritage.org/AZAPo5xoSDG-SUZb9jiDiQ# 
- SWH Code of conduct: https://gitlab.softwareheritage.org/swh/devel/swh-docs/-/blob/master/CODE_OF_CONDUCT.md

## Workshop details
### What are the goals of the workshop? 
The goals of each activity will be presented by its lead. 
The common goal of the activities is to enrich the set of SWH materials. 

### What are the prerequisites to attend? 
Being part of the Software Heritage community.

### Will the work done during the activity be presented to the other attendees? 
Yes, for each activity, the lead will need:
- someone who will take notes with the template (cf. quick links)
- someone who will contribute to the report back in the plenary session

The same person can play these 2 roles. 

### Do I need a laptop? 
You can come without a laptop but a laptop per group will be useful.

## Getting started

### Before the workshop
1. Step 1: Create a SWH Gitlab account 
2. Step 2: Comment this issue https://gitlab.softwareheritage.org/outreach/community/workshop-2023/-/issues/2
3. Step 3: Connect to https://matrix.to/#/#swh:matrix.org


## Contributing
All contributions to this repository are under CC-BY-4

## Contributors
### Workshop organizers: 
- Morane Gruenpeter
- Sabrina Granger
- Lunar Bobbio
- Benoît Chauvet
- Italo Vignoli
- Joenio Marques da Costa 

### Workshop participants:
Please add yourself to this list as contributors.


## License
CC-BY-4

