# Software Stories checklist

*After being reviewed and slightly amended during the workshop at the 2023 Software Heritage Symposium.*

Final 2021 report: https://www.overleaf.com/7443654297ykjypvncmdys


## Workflow

from [Final 2021 report](https://www.overleaf.com/7443654297ykjypvncmdys)

> 1. The curator creates a workbench repository using the SWH template (part of the SWHAP workflow).
> 2. The curator uploads files into the raw materials directory.
> 3. The curator adds metadata for the software in the form of a codemeta.json file.
> 4. The curator searches Wikidata to see if an item for the software title exists.
> 5. If no item is found, the curator creates a new item.
> 6. The curator adds statements to the Wikidata item based on the codemeta.json file as well as any other available information using properties related to software.
> 8. The curator uploads images related to the software to Wikimedia Commons.
> 9. If there is only one image, the curator visits the Wikidata item for the software title and uses P18 to connect the image to the Wikidata item.
> 9. If there are multiple images, the curator creates a category for the software title in Wikimedia Commons and the returns to Wikidata and uses P373 to connect the category to the Wikidata item.
> 10. If there are images of files that were created with the software title, the curator creates a category in Wikimedia Commons for files created with the software title. The curator then returns to Wikidata and uses P7861 to connect the category for files created with the software to the Wikidata item.
> 11. The curator selects an interesting code fragment or directory and assigns anchor in Software Heritage archive.
> 12. The curator uses the Stories Services Publisher Workspace to add a SWH source code moment to the story and enters the URL or string of the anchorso that the moment will display the relevant fragment or directory.
> 13. The curator saves the story.


## SWHAP

- [ ] The curator creates a workbench repository using the SWH template (part of the SWHAP workflow).
- [ ] The curator uploads files into the raw materials directory.
- [ ] The curator adds metadata for the software in the form of a codemeta.json file.

## Wikidata

- [ ] The curator searches Wikidata to see if an item for the software title exists
- [ ] If no item is found, the curator creates a new item or uses Cradle
https://cradle.toolforge.org/#/subject/software_heritage_software_title

- [ ] The curator adds statements to the Wikidata item based on the codemeta.json file as well as any other available information using properties related to software.

## Source code repository

- [ ] Add the SWH repository to  wikidata's **source code repository** statement

![](b52c4678-1beb-4578-9af2-fca0fe260c9b.png)

## The landing page

- wikipedia entry
- blogpost on SWH (I have a preference for the blogpost entry)

- [ ] Blogpost entry (according to template)

**Blogpost template:**
- What is XXXXX and what it does
- Who developed it, when and  where
- What need it does reply to
- Versions 
- Users, diffusion 
- Optional, if still developed/in use Current and Future

## Media gallery moment

- [ ] The curator uploads images related to the software to Wikimedia Commons.
- [ ] If there is only one image, the curator visits the Wikidata item for the software title and uses [P18](https://www.wikidata.org/wiki/Property:P18) (“image of relevant illustration of the subject”) to connect the image to the Wikidata item.
- [ ] If there are multiple images, the curator creates a category for the software title in Wikimedia Commons and the returns to Wikidata and uses [P373](https://www.wikidata.org/wiki/Property:P373) (“name of the Wikimedia Commons category containing files related to this item”) to connect the category to the Wikidata item.
- [ ] If there are images of files that were created with the software title, the curator creates a category in Wikimedia Commons for files created with the software title. 
- [ ] The curator then returns to Wikidata and uses [P7861](https://www.wikidata.org/wiki/Property:P7861) (“files created with program”) to connect the category for files created with the software to the Wikidata item.

### PICTURES

- [ ] logo(s) ([P154](https://www.wikidata.org/wiki/Property:P154) “logo image”)
- [ ] people at work
- [ ] buildings
- [ ] group pictures
- [ ] conferences
- [ ] screenshots

### VIDEOS

- [ ] presentation
- [ ] interviews
- [ ] demos

## Timeline moment

- [ ] Versions of software: add new **software version identifiers** to wikidata

![](6572f033-894d-4d2f-9826-2bc8dbfe91bc.png)

- [ ] add other important dates.

**Question**
how do I achieve this? Is this via the publisher interface or is it via Wikidata?

![](18dd76a4-5157-49ac-a45e-483d03608e21.png)

> Wikidata does not use personal communication as a reference. Referencing a URL is always possible. But it is also possible to create an item for a published document. ==XXX: verify all of this is right, the spoken discussion was fast==


## Map moment

- [ ] places (To be defined better, perhaps)

## People moment

- [ ] Preliminary work:
  - names of people that were relevant to the project;
  - date of birth, occupation
  - the dates they worked for the project;
  - their roles in the project and their occupation;
  - a contact email or number (when available) in order to reach them if needed.
  - people pictures (at least one) (they will be taken from wikidata, so they won't reflect time the software were designed)
- [ ] check if people are already on  Wikidata
- [ ] if not create QID
- [ ] add DOB and other relevant info (profession, roles in the project etc.)

## Library moment 

(Missing from [Final 2021 report](https://www.overleaf.com/7443654297ykjypvncmdys))

Preliminary work:
- look for references, publications and any other kind of document available.

### Publications

- as references 
- if rights ok to be published as pdfs or images (scans)
- [ ] manuals
- [ ] books
- [ ] articles & papers

### Other documents

- [ ] flyers
- [ ] list of software versions
- [ ] keynotes
- [ ] listings (paper prints of source code, maybe annotated)
- [ ] schematics/diagrams/flowcharts
- [ ] notes
- [ ] internal communications
- [ ] correspondence

> One option for adding more content to the Library moment is to create Wikidata items for any publications relating to MAGMA-Lisp. For example, I created a new item [1] for a scientific article about it and then used "main subject" to connect it to the MAGMA-Lisp item. The article now appears in the Library moment, and it is also now on the timeline.
> 1. https://www.wikidata.org/wiki/Q115358242

## SWH source code moment

- [ ] publish/link the SWHAP
- [ ] The curator selects an interesting code fragment or directory and assigns anchor in Software Heritage archive.
- [ ] The curator uses the Stories Services Publisher Workspace to add a SWH source code moment to the story and enters the URL or string of the anchor so that the moment will display the relevant fragment or directory.

## Publication of the Software Story

- [ ] The curator saves the story.


## Scilab Software Story (Q828742)

https://swh.stories.k2.services/inria/Q828742

## Magma-Lisp (Q114137083)

https://swh.stories.k2.services/pisa/Q114137083

## Le Lisp (Q6507148)













