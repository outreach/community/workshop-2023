# 2023-02-07 Notes ⏵ SWHAP & Software Stories - discuss the curator checklist


## Leads

Elisabetta Mori
Kat Thornton




## List of attendees

- Valentin
- Jayesh
- Lunar
- Carlo Montogero
- Camille (Netherlands Institute for Sound and Vision)
- Marc-Etienne
- Elisabetta
- Kat


## Misc

- Software Stories does not really work on mobile phones currently.
- Pick a naming convention for Wikidata. It should be more generic than just Software Heritage but it would be its main usage. For the name, consulting Wikimedia Commons rule is a prerequisites.
- Identified more relevant Wikidata categories. Added identifiers, names and links to the relavant Wikidata pages. 
- Adding all released versions to Wikidata requires sources (URL or referenced documents).
- In the “relevant people” section, the pictures wil be take from Wikidata and not pictures of the time the software was developped.
- Hosting images is an issue. Uploading images to Wikimedia Commons requires being the author of the picture.
- The more material is put in Wikidata and Wikimedia Commons the more is it automatic to create the story. Adding material manually can allow for a encompassing story but makes it more fragile and requires more work. 
- What qualifies as a Software Story? Who is a curator? Good questions! The idea of the checklist is to enable anyone with a rough idea of what we are talking about to be able to collect materials in an homogenous form. Is it expected that institutions working in the field of cultural preservation would apply the checklist? 
- What should we do about copyrights?

## 3 key messages for the report back

- Idea #1
    - No real feedback for now from curators about using Wikidata interface to enter information as it only has been Elisabetta and Carlo for the time being. 
    - Anyway, the most difficult thing is social and not technical: working with people. The ones with knowledge sometimes don’t reply to emails, or don’t want to talk, or a willing to talk but only face-to-face, or have signed NDAs, or just suddently become unavailable or disappear.
- Idea #2
    - [Craddle](https://www.wikidata.org/wiki/Wikidata:Cradle) is a Wikidata tool to create forms that can be used by end users to enter data more easily. Kat made one for curators: https://cradle.toolforge.org/#/subject/software_heritage_software_title
- Idea #3
    - For the map moment, if the place says “Italy”, the location marker is put on “the middle of nowhere” or more accurately, the “barycenter of Italy”, which is not very useful.
- Next steps:
    - Elisabetta creates a new version of the checklist.
    - Trying the checklist would be the best way to see if it is usable and if it still 

