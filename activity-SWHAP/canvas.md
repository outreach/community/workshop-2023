# SWHAP & Software Stories - discuss the curator checklist

Lead: Elisabetta Mori & Lunar

Prerequisite: read the SWHAP guide/know the SWHAP principles? 
Total duration: 1h

**Main goal: Produce a useful checklist for software curators in their Software Story creation**

[TOC]

## Description

Software Heritage in collaboration with the sciencestories.io team and the University 
of Pisa are introducing a new way to showcase software, that can be accessible to a 
wide range of software enthusiasts without any technical background, Software Stories. 
The project is supported by UNESCO as part of the shared mission to collect, preserve 
and share source code as precious asset of humankind [1]. The Software Stories interface 
is designed to highlight materials about a software title in a visual manner, similar 
to a digital software museum. The engine provides a semi-automatic tool for curators 
to create the presentation layer of the Software Heritage Acquisition Process (SWHAP)[2]
allowing curators to generate a multimedia overview of a landmark legacy software title. 
For the prototype, the University of Pisa has provided three software titles that were 
curated in 2019 during the creation of the SWHAP. These software titles were collected
and curated in GitHub and then archived in Software Heritage. For the presentation layer 
the collected materials and metadata were deposited in Wikidata and Wikimedia Commons. 
You can visit https://stories.softwareheritage.org to check the Pisa collection and 
watch the demonstration video online on https://youtu.be/s6uVdRDh5Xk.

The Software Heritage Acquisition Process (SWHAP) is a protocol to collect, preserve, 
curate and present legacy software of historical or scientific relevance [2]. It
was established in 2019 as a result of a collaboration between Software Heritage
(SWH), the University of Pisa and UNESCO. Recovering legacy source code, where
the human readable knowledge is kept, is a delicate task. It requires a careful
methodology to collect and reconstruct the innovative creation of its authors which
might reveal a major scientific discovery.
In the SWHAP protocol, different actors have different tasks to collect the legacy
software and all the physical and digital artifacts that are related to the software
itself or its creators. One important aspect of preserving the software is also 
preserving the contextual information and artifacts in which it was built. The context
of the software is a window to the past, which is essential to understand the 
software and its story. The story of the software is the one that will be told 
to the next generations. 

Therefore, curating the story around the software is as important as
curating the software itself.
Software Heritage in collaboration with the https:\sciencestories.io team and
the Pisa university are introducing a new way to showcase software, which is accessible 
for a wide range of software enthusiasts without any technical background,
the software story. Each software story is a collection of pages, called moments,
where different elements about a software are visualised, like in a virtual software
museum.

Description was taken from [4]

## Useful Links
- SWH stories diagrams https://hedgedoc.softwareheritage.org/D1qoMhs8QBKPa1oN38ejWA?both
- UNESCO final report https://hal.archives-ouvertes.fr/hal-03483982/


SWHAP links:
- SWHAP guide: https://github.com/SoftwareHeritage/swhapguide




## Canvas of the activity
The discussion will be focused on the use case "As an historian or a software curator, I need a "easy to use" checklist to be able to create a Software Story following the SWHAP protocol?"

## Notes

- [Checklist amended during collective review](full-checklist.md)
- [Notes taken](notes.md)

## Next steps

- Elisabetta creates a new version of the checklist. ℹ️  [**Now available!**](2023-03-10-SWHAP_and_Stories_final_report_-_Annex_C_Software_Stories_Checklist.pdf)
- Organise test sessions for the checklist to see if it is understandable and usable

## References

[1] Expert Group Report. Paris call: Software source code as heritage for sustainable development. Available from https://unesdoc.unesco.org/ark:/48223/pf0000366715, 2019.

[2] Laura Bussi, Roberto Di Cosmo, Carlo Montangero, and Guido Scatena. The software heritage acquisition process. Technical Report CI-2019/WS/8, UNESCO, Università di Pisa, Inria, 2019. https://unesdoc.unesco.org/ark:/48223/pf0000371017

[3] Laura Bussi, Roberto Di Cosmo, Carlo Montangero, and Guido Scatena. Preserving landmark legacy software with the software heritage acquisition process.
In iPres2021 - 17th International Conference on Digital Preservation, Beijing,
China, 2021.

[4] Morane Gruenpeter, Roberto Di Cosmo, Katherine Thornton, Kenneth Seals-Nutt, Carlo Montangero, et al.. Software Stories for landmark legacy code. [Research Report] Inria. 2021. (hal-03483982)[https://hal.science/hal-03483982/]


