---
tags: community event
---
# Empathy Map / Name of the non-technical audience type: group 1

## Name of the note taker
*First and last name please (in case we'd need further information from you after the activity. Thanks for your understanding):* 

---
Note taking:
- linguistic pallette (essay: https://the.webm.ink/getting-back-to-a-social-frame, referenced academic: [George Lakoff](https://george-lakoff.com/))
- Free and Open Source discussion
    - price frame
- what is the frame
- return on investement discussion
- software enabling 
- Start with a social frame rather than a price("free") or technical ("source code") frame, and introduce those concpets later within the conceptual metaphorical frame of social value 
- Italo: Free software is not about code

- How can we communicate?
    - examples that will be linked to the daily lives
    - software by the people for the people
    -  find the words about Software Heritage
- How do fiction authors explain technology?
    - They find analogies and metaphors
        - Terry Pratchett: https://discworld.fandom.com/wiki/Technology_of_the_Discworld
        - Star Trek: https://en.wikipedia.org/wiki/Technology_in_Star_Trek
        - Douglas Adams: https://hitchhikers.fandom.com/wiki/List_of_technology_in_the_Hitchhiker%27s_Guide_to_the_Galaxy
- Maybe write a children's book: https://fsfe.org/activities/childrensbook/index.en.html (maybe not directly about Software Heritage itself, but about how it feels to have Software that we know is safely archived and available? how it feels to have software disappear? How it feels to have to re-invent something that was already invented again and again?)

- Anecdote: Caolán @ Libre office


- Idea: On the SWH website, showcasing examples of software that people have been using on a daily basis, and are being archived on SWH. Maybe a Software Story?


- People don't see software as cultural heritage? What are the technical blocks that prevent people from teaching / understanding Software Heritage. The SWH website jumps to Source Code really fast.

- Presenting what is software? That's actually hard: where does it start, where does it end? An Operating System is software, but what about the software that runs on the OS? 

- Showing people that there is software underpinning every bit of their daily life; e.g. "a kindle is just like a book" -> no, it's actually software








---

[TOC]

## 1. WHO are we empathizing with? 
Who is the person we want to understand? 
What is the situation they are in? 
What is their role in the situation? 


- Non-tech savvy journalists



## 2. What do they need to DO? 
What do they need to do differently? 
What job(s) do they want or need to get done? 
What decision(s) do they need to make? 
How will we know they were successful? 


- Tasked by their editor in chief to write about Software Heritage
- collect information about Software Heritage in a short time



## 3. What do they SEE? 
What do they see in the marketplace? 
What do they see in their immediate environment? 
What do they see others saying and doing? 
What are their watching and readings?

- a Google search result for Software Heritage
- the first result is https://www.softwareheritage.org/
- maybe they end up on https://docs.softwareheritage.org/ ? Probably not.
- maybe they click on the "Discover our mission" link? > https://www.softwareheritage.org/mission/



## 4. What do they SAY? 
What have we heard them say? 
What can we imagine them saying? 

- They will probably navigate the whole website if they have to write an article about it.
- or say: oh, it's technical, non-interesting stuff.


## 5. What do they DO?
What do they do today? 
What behavior have we observed? 
What can we imagine them doing? 




## 6. What do they HEAR?
What are they hearing others say? 
What are they hearing from friends? 
What are they hearing from colleagues? 
What are they hearing second-hand? 




## 7. What do they THINK and FEEL?
**PAINS**
What are their fears, frustrations, and anxieties?




**GAINS**
What are their wants, needs, hopes and dreams? 



---
What other thoughts and feelings might motivate their behavior? 




---

# Original layout
https://gitlab.softwareheritage.org/outreach/community/workshop-2023/-/blob/main/activity-users-materials/Empathy-Map-006-PNG.png 
![](https://hedgedoc.softwareheritage.org/uploads/21a1665e-3d5d-4cef-be90-39e6c4ff6ccf.png)




