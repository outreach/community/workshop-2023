# Create materials for non-technical audiences
Proposal of canvas for the community event activity: 

[TOC] 

## Introduction 
* Contents: main stakes, how to reach non-technical audiences in a more efficient way? 
* lead: Italo
* Plenary: 10'
* Materials: Italo's slides

## Choose a note taker and a presenter for the report back
Ask the attendees if someone could: 
- [ ] take notes, using the template : https://hedgedoc.softwareheritage.org/xiGUg4cmQ16BlmWpbeMa4Q# 
- [ ] present the work done during the report back in plenary session

1 or 2 persons are needed. 

## Quick exchanges to prepare the activity
* Contents : feedbacks from the audience on the main categories of the non-technical audiences we try to reach, based upon Italo's slides: do we need to adapt these categories, to add categories? 
The aim of this short step is to establish the final list of profiles to work on during the activity. The activity can't start without the final list of the profiles.    
* Lead (the person who will deal with the distribution of speech among attendees): SWH team member
* Plenary: 5' 
* No material

## Introduction of the exchanges in sub-groups 
* Contents : explanations on the objective for each group and handing-out of the materials that will be used by the sub-groups. Each group chooses to work on one type of non-technical audience, filling an empathy map, in order to get a common canvas to facilitate the discussion with the other sub-groups right after. 
The aim of this sequence for each sub-group is to provide to the other sub-groups a synthesis of their discussion via the big printed empathy map. 
* Lead (the person who will explain how the discussion will happen): : SWH team member
* Plenary: 5' 
* Materials: empathy map ; the editable file (short URL to the file, no email sent) and printed versions (A4 and A3) will be provided. 

## Discussion in sub-groups
* Contents: discussion based upon the empathy map.
To prepare the final exchanges step, each sub-group produces a synthesis of its discussion on a big printed empathy map that will be displayed like a poster. 
At the end of the alloted period of time, each sub-group displays its poster.
* Lead (the person in charge of the time management, not the person in charge of the exchanges in the sub-groups): SWH team member
* Sub-groups: 20'
* Materials: 
- [ ] empathy map (printed version A4 and A3 and editable file): A4 for working version ; A3 for final version
- [ ] empathy map editable items (pad) : https://gitlab.softwareheritage.org/outreach/community/workshop-2023/-/blob/4a57d8bebf20a9cfee86835eedb240486ddbb82a/activity-users-materials/editable-empathy-maps.md
   
## Visit of the sub-groups posters
* Contents: reading the posters
* Lead (the person in charge of the time management): SWH team member
* Plenary, each one is free to pick up the information deemed relevant: 10'
* Materials: empathy map A3

## Final exchanges: Q&A and closing of the activity
* Contents: discussion based upon the empathy map filled by each sub-group. It's a Q&A time between participants. 
As the contents of each sub-group discussion are summarized on the empathy map poster, there is no debriefing phase by subgroup. We go directly to the questions. 
* Lead (the person in charge of time management and the distribution of speech): SWH team member 
* Plenary : 10'
* Materials: empathy map

## Total duration
1h

## Ultimate thoughts (not included in the workshop timing, additionnal activity)
While leaving the workshop, during the coffee break, etc. 
Optional: provide a paper board on which people can put some contents. 
Put the beginning of a sentence to inspire people, to incite them to share their thoughts. (e.g.: "Something that always surprises me is ..." ; "I wish I could ...")
Take a picture of the paper board to share it with all the group at the end of the session. 

## Materials to prepare before the event
- [X] introduction slides ; Italo
- [X] put online an editable version of the empathy map to provide a short URL during the workshop
- [X] print copies of the empathy map (for draft version: A4)
- [X] print copies of the empathy map in A3
- [ ] provide pens to attendees
- [ ] ask for a paperboard to Alison (Unesco)
- [ ] have something to hold the posters:
    - [ ] in terms of space in the meeting room
    - [ ] in terms of tape, etc.

