# Editable empathy maps items

You'll find here an editable version of the fields of the empathy maps. 
This template should be completed in parallel of the printed poster. It will help SWH to foster its contents, thanks in advance !

If you need a new template, please, copy paste the items in a new pad. 
Blank template to be copied-pasted: 
https://hedgedoc.softwareheritage.org/brVqDOQ3QdWQgU7ELmeWTA# 

---


Target audience 1
https://hedgedoc.softwareheritage.org/Ts72Sok7SwSnUkgssncSAg#

Target audience 2
https://hedgedoc.softwareheritage.org/Tzh6n5MtTXqKKDKPMqjEZg# 


Target audience 3
https://hedgedoc.softwareheritage.org/1EbAjV0PSaGxZqF7t7UVWQ#

Target audience 4
https://hedgedoc.softwareheritage.org/e9bzVFpLSiabJVtU66VhGg#

Target audience 5
https://hedgedoc.softwareheritage.org/VXNhZQH5QlmSrSMfxtZ0PA# 





